console.log("Hello World")
/*
	JSON
		JavaScript Object Notation
		-also used in other programming language

		Syntax:

		{
			"propertyA" : "valueA"
			"propertyB" : "valueB"
		};
		
		JSON are wrapped in curly braces
		Properties/Keys are wrapped in double quotations
*/

	let sample1 = `
		{
			"name" : "Cardo Dalisay",
			"age" : 20,
			"address" : {
				"city" : "Quezon City",
				"country" : "Philippines"
			}

		}
	`;

	console.log(sample1);

	//mini activity
	//create a variable that will hold a JSON and create a person object
		//log and send ss

	let sample2 = `
		{
			"name" : "Ash Ketchum",
			"age" : 35,
			"address" : {
				"city" : "Tralala",
				"country" : "Zimbabwe"
			}

		}
	`;

	console.log(sample2);
	console.log(typeof sample1);//string

	//Are we able to turn a JSON into a JS Object?
	//JSON.parse() - will return the JSON as an Object

	console.log(JSON.parse(sample1));
	console.log(JSON.parse(sample2));

	//JSON Array 
	//JSON Array is an array of JSON

	let sampleArr = `
		[
			{
				"email" : "sinayangmoko@gmail.com",
				"password" : "feb14",
				"isAdmin" : "false"

			},
			{
				"email" : "dalisay@cardo.com",
				"password" : "thecountryman",
				"isAdmin" : "false"
			},
			{
				"email" : "jsonv@gmail.com",
				"password" : "friday13",
				"isAdmin" : "true"				
			}
		]
	`;

	console.log(sampleArr);
	console.log(typeof sampleArr);//string

	//Can we use Array Methods on a JSON Array?
	//No. Because a JSON is a string

	//so what can we do to be able to add more items/objects into our sampleArr?

	//PARSE the JSON and save the JS array and save it in a variable

	let parsedSampleArr = JSON.parse(sampleArr);
	console.log(parsedSampleArr);
	console.log(typeof parsedSampleArr);//object - array

	//Can we now delete the last item in the JSON Array?

	console.log(parsedSampleArr.pop());
	console.log(parsedSampleArr);

	//if for example we need to send this data back to our client/front end it should be in JSON format

	//JSON.parse() does not mutate or update the original JSON
	//we can actually turn a JS object into a JSON
	//JSON.stringify() - this will stringify JS objects as JSON

	sampleArr = JSON.stringify(parsedSampleArr);
	console.log(sampleArr);

	//Database (JSON) => Server/API (JSON to JS Object to process) => sent as JSON => frontend/cient

	//Mini Activity
	

	let jsonArr = `
	[
		"pizza",
		"hamburger",
		"spaghetti",
		"shanghai",
		"hotdog stick on a pineapple",
		"pancit bihon"
	]
	`
	console.log(jsonArr);
	let parsedJsonArr = JSON.parse(jsonArr);

	parsedJsonArr.pop();
	parsedJsonArr.push("ice cream");

	jsonArr = JSON.stringify(parsedJsonArr);
	console.log(jsonArr);



	//Gather User Details

	let firstName = prompt("What is your first name?");
	let lastName = prompt("What is your last name?");
	let age = prompt("What is your age?");
	let address = {
		city: prompt("Which city do you live in?"),
		country: prompt("Which country does your city address belong to?")
	};

	let otherData = JSON.stringify({

		firstName: firstName,
		lastName: lastName,
		age: age,
		address: address
	})

	console.log(otherData);